# README

Ruby on Rails app template, configured with SQLite3 (development & test) and PostgreSQL (production).

## App Information

App Name: rails-docker

Created: February 2024

Creator: Christian McTighe

Email: mctighecw@gmail.com

Repository: [Link](https://gitlab.com/mctighecw/rails-docker)

## Tech Stack

- Ruby (3.3.0)
- Rails (7.1.3)
- SQLite3
- PostgreSQL
- Docker

## To Run

### Locally

Get app info:

```sh
$ rails about
```

Update gems, if necessary:

```sh
$ bundle update
```

Install gems:

```sh
$ bundle install
```

Start dev server:

```sh
$ bin/rails server
```

### With Docker

1. Fill out `.env` file values

2. Build Docker image and start container

```sh
# Development
$ docker-compose up -d --build
or
# Production
$ docker-compose -f docker-compose-prod.yml up -d --build
```

## Setup Database (Production)

```sh
$ docker exec -it rails-docker bash

> rake db:create
> rake db:migrate
> rake db:seed
```

Last updated: 2025-01-16
